package ui

import (
	"fmt"
	"regexp"
	"strconv"

	"andrewbruno.com/datediff/calendar"
)

const (
	regexDaY1   = "(?P<d1>[0][1-9]|[1][0-9]|[2][0-9]|3[01])" // Allow values from 01 to 31, returned as group d1 in map
	regexMonth1 = "(?P<m1>0[1-9]|1[012])"                    // Allow values from 01 to 12, returned as group m1 in map
	regexYear1  = "(?P<y1>19[0-9][0-9]|200[0-9]|2010)"       // Allow values from 1900 to 2010, returned as group y1 in map

	regexDaY2   = "(?P<d2>[0][1-9]|[1][0-9]|[2][0-9]|3[01])" // Allow values from 01 to 31, returned as group d2 in map
	regexMonth2 = "(?P<m2>0[1-9]|1[012])"                    // Allow values from 01 to 12, returned as group m2 in map
	regexYear2  = "(?P<y2>19[0-9][0-9]|200[0-9]|2010)"       // Allow values from 1900 to 2010, returned as group y2 in map
)

var (
	// This regex expression is not 100% full proof, but offers a first layer of defence removing many invalid dates
	// E.g. Outside the year range, or days greater than 31 or months greater than 12.
	//  However, it will allow dates like 1901 02 31 - so, we still need to validate the date as a second layer.
	regexInputStr = fmt.Sprintf("^%s %s %s, %s %s %s$", regexDaY1, regexMonth1, regexYear1, regexDaY2, regexMonth2, regexYear2)

	// Local variable that holds the compiled Regex
	// This allows us to only create once, and use many times
	regExpInput = regexp.MustCompile(regexInputStr)
)

// Input holds the parsed input, as per Date 1 and Date 2 passed in
type Input struct {
	y1 string
	m1 string
	d1 string
	y2 string
	m2 string
	d2 string
}

// Parser interface defining the to implement this method
type Parser interface {
	ParseInputDates(string) (*calendar.Date, *calendar.Date, error)
}

// ParseInputDates parses the input, returning two valid date objects or an error.
func ParseInputDates(input string) (*calendar.Date, *calendar.Date, error) {

	inputObj, err := parseInput(input)

	if err != nil {
		return nil, nil, err
	}

	// No need to check for parse errors, as "0" is an invalid day, month or year
	// and NewDate will check whether the passed in values are valid anyway
	y, _ := strconv.ParseInt(inputObj.y1, 10, 16)
	m, _ := strconv.ParseInt(inputObj.m1, 10, 8)
	d, _ := strconv.ParseInt(inputObj.d1, 10, 8)

	date1 := calendar.NewDate(int16(y), uint8(m), uint8(d))
	if date1 == nil {
		return nil, nil, fmt.Errorf("Invalid Input, unable to parse first date")
	}

	y, _ = strconv.ParseInt(inputObj.y2, 10, 16)
	m, _ = strconv.ParseInt(inputObj.m2, 10, 8)
	d, _ = strconv.ParseInt(inputObj.d2, 10, 8)

	date2 := calendar.NewDate(int16(y), uint8(m), uint8(d))

	if date2 == nil {
		return nil, nil, fmt.Errorf("Invalid Input, unable to parse second date")
	}

	return date1, date2, nil
}

// parseInput takes in the raw input from CLI / User and retuns the individual elements.
// It does a first pass at validating the input via regex, but dates may still be invalid.
// Expects the input to be in the format: DD MM YYYY, DD MM YYYY
// If the input is not in the above format, then an error is returned.
func parseInput(input string) (*Input, error) {
	match := regExpInput.FindStringSubmatch(input)

	if len(match) != 7 {
		return nil, fmt.Errorf("Invalid input")
	}

	inputObj := &Input{}

	// Let's extract the groups
	for i, name := range regExpInput.SubexpNames() {
		if i > 0 && i <= len(match) {
			switch name {
			case "y1":
				inputObj.y1 = match[i]
			case "m1":
				inputObj.m1 = match[i]
			case "d1":
				inputObj.d1 = match[i]
			case "y2":
				inputObj.y2 = match[i]
			case "m2":
				inputObj.m2 = match[i]
			case "d2":
				inputObj.d2 = match[i]
			}
		}
	}

	return inputObj, nil
}

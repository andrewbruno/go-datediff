// This package tests externals only
package ui_test

import (
	"testing"

	"andrewbruno.com/datediff/calendar"
	"andrewbruno.com/datediff/ui"
)

func TestParseInputDatesValid(t *testing.T) {
	cases := []struct {
		input string
		date1 *calendar.Date
		date2 *calendar.Date
	}{
		{
			input: "08 01 1995, 24 12 2005",
			date1: calendar.NewDate(1995, 1, 8),
			date2: calendar.NewDate(2005, 12, 24),
		},
		{
			input: "15 04 1969, 12 09 1945",
			date1: calendar.NewDate(1969, 4, 15),
			date2: calendar.NewDate(1945, 9, 12),
		},
		{
			input: "01 01 1900, 31 12 2010",
			date1: calendar.NewDate(1900, 1, 1),
			date2: calendar.NewDate(2010, 12, 31),
		},
	}

	for _, c := range cases {
		date1, date2, err := ui.ParseInputDates(c.input)

		if err != nil {
			t.Errorf("Received error [%s] for input %s", err.Error(), c.input)
			continue
		}

		if date1 == nil {
			t.Errorf("Received first date as nil for input %s", c.input)
			continue
		}

		if date2 == nil {
			t.Errorf("Received second date as nil for input %s", c.input)
			continue
		}

		if date1.String() != c.date1.String() {
			t.Errorf("Received first date as [%s] but expected [%s] for input %s", date1, c.date1, c.input)
			continue
		}

		if date2.String() != c.date2.String() {
			t.Errorf("Received second date as [%s] but expected [%s] for input %s", date2, c.date2, c.input)
			continue
		}
	}

}

func TestParseInputDatesError(t *testing.T) {
	cases := [...]string{
		"lalalal",
		"01 1 1900, 02 02 2000",   // Not padded
		"30 02 1900, 02 02 2000",  // Invalid date 30 Feb
		"01 01 1900, 02 29 1999",  // Not a leap year
		"01 01 1900, 02 02 2011",  // Out of range
		"01 01 1900 02 02 2000",   // no comma
		"01 01 1900,  02 02 2000", // too many spaces after comma
		"31 12 1899, 01 01 2011",  // both dates invalid, out of range
		"31 06 1900, 29 09 2010",  // first date invalid, BUT regex passed
		"30 06 1900, 31 09 2010",  // second date invalid, BUT regex passed
		"31 06 1900, 31 09 2010",  // both dates invalid, BUT regex passed
	}

	for i := range cases {
		_, _, err := ui.ParseInputDates(cases[i])

		if err == nil {
			// Got an error, but did not expect one
			t.Errorf("Expected an error for input [%s], but did not receive one.", cases[i])
			continue
		}
	}

}

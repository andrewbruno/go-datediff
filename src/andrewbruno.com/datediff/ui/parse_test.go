package ui

import (
	"fmt"
	"testing"
)

func TestParseInput(t *testing.T) {

	// compare is a helper test function to assist in comparing two Input object
	compare := func(input1 *Input, input2 *Input) bool {
		if input1 == nil && input2 == nil {
			// both nil, so technically the same
			return true
		}

		if (input1 == nil && input2 != nil) || (input1 != nil && input2 == nil) {
			// one is nill, but the other is not
			return false
		}

		if input1.y1 == input2.y1 && input1.m1 == input2.m1 && input1.d1 == input2.d1 &&
			input1.y2 == input2.y2 && input1.m2 == input2.m2 && input1.d2 == input2.d2 {
			return true
		}

		return false
	}

	cases := []struct {
		input  string
		output *Input
		err    error
	}{
		{
			input: "08 01 1995, 24 12 2005",
			output: &Input{
				y1: "1995",
				m1: "01",
				d1: "08",
				y2: "2005",
				m2: "12",
				d2: "24"},
			err: nil,
		},
		{
			input: "10 01 1995, 24 12 2005",
			output: &Input{
				y1: "1995",
				m1: "01",
				d1: "10",
				y2: "2005",
				m2: "12",
				d2: "24"},
			err: nil,
		},
		{
			input: "15 04 1969, 12 09 1945",
			output: &Input{
				y1: "1969",
				m1: "04",
				d1: "15",
				y2: "1945",
				m2: "09",
				d2: "12"},
			err: nil,
		},
		{
			input:  "15 04 1969 12 09 1945",
			output: nil,
			err:    fmt.Errorf("Invalid input"),
		},
		{
			input:  "15 04 196, 12 09 1945",
			output: nil,
			err:    fmt.Errorf("Invalid input"),
		},
		{
			input:  "15 04 1969, 12 9 1945",
			output: nil,
			err:    fmt.Errorf("Invalid input"),
		},
		{
			input:  "15 04 1969, 2 09 1945",
			output: nil,
			err:    fmt.Errorf("Invalid input"),
		},
		{
			input:  "15 13 1969, 02 09 1945",
			output: nil,
			err:    fmt.Errorf("Invalid input"), // 13th month
		},
		{
			input: "99 99 3999, 12 09 1945",
			output: &Input{
				y1: "3999",
				m1: "99",
				d1: "99",
				y2: "1945",
				m2: "09",
				d2: "12"},
			err: fmt.Errorf("Invalid input"), // Out of bounds
		},
		{
			input: "01 01 1900, 31 12 2010",
			output: &Input{
				y1: "1900",
				m1: "01",
				d1: "01",
				y2: "2010",
				m2: "12",
				d2: "31"},
			err: nil,
		},
		{
			input: "O1 01 1900, 31 12 2010",
			output: &Input{
				y1: "1900",
				m1: "01",
				d1: "01",
				y2: "2010",
				m2: "12",
				d2: "31"},
			err: fmt.Errorf("Invalid input"), // Letter O instead of 0 used
		},
		{
			input: "30 02 1900, 31 12 2010",
			output: &Input{
				y1: "1900",
				m1: "02",
				d1: "30", // Invalid, but OK for parser.
				y2: "2010",
				m2: "12",
				d2: "31"},
			err: nil, // Parser does not verify if dates are legitimate
		},
		{
			input:  "00 13 1969, 02 09 1945",
			output: nil,
			err:    fmt.Errorf("Invalid input"), // 00 not allowed for day
		},
	}

	for _, c := range cases {
		o, err := parseInput(c.input)

		if c.err == nil && err == nil {
			if !compare(c.output, o) {
				t.Errorf("Parsing error for input [%s].  Received [%s %s %s, %s %s %s]", c.input, o.d1, o.m1, o.y1, o.d2, o.m2, o.y2)
				continue
			}
		}

		// Either c.err or err or both received an error
		if c.err != nil && err != nil {
			// They both received an error, so a match.
			if err.Error() != c.err.Error() {
				t.Errorf("Expected an error for input [%s], but the error messages differ. Expected:[%s], Received:[%s]", c.input, c.err.Error(), err.Error())
				continue
			}
		} else {
			// Only one was in error
			if c.err != nil && err == nil {
				t.Errorf("Did not error, but expected an error for input [%s]", c.input)
				continue
			}

			if c.err == nil && err != nil {
				t.Errorf("Unexpected error [%s] for input [%s]", err.Error(), c.input)
				continue
			}
		}
	}
}

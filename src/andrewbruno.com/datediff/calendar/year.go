package calendar

var (
	_minYear int16 = 1900 // min year allowed
	_maxYear int16 = 2010 // max year allowed
)

// ValidYear verifies that the year is a valid supported year.  If not, returns false.
func ValidYear(year int16) bool {
	if year < _minYear || year > _maxYear {
		return false
	}

	return true
}

// LeapYear returns true if the year is a leap year, false if not.
// The definition of a leap year is:
//
// if (year is not divisible by 4) then (it is a common year)
// else if (year is divisible by 100 but not divisible by 400 ) then (it is a common year)
// else (it is a leap year)
func LeapYear(year int16) bool {
	// Leap years occur every 4 years, and years that are evenly divisible by 4
	// e.g. 2004, 1900 (technically NOT, but passes first test)
	// so quickly ignore what is NOT divisible by 4
	if year%4 != 0 {
		return false // common year
	}

	// From this point on, it "may" be a leap year

	// The Gregorian calendar says that a year that is evenly divisible by 100
	// (for example, 1900) is a leap year only if it is also evenly divisible by 400
	// which its not.  On the other hand, 1600 and 2000 are.
	if year%100 == 0 && year%400 != 0 {
		// Century = yes, but not divisable by 400
		return false // common year
	}

	return true // It must be a leap year
}

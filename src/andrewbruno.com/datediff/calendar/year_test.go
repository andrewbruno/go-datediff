package calendar

import (
	"testing"
)

func TestValidYear(t *testing.T) {
	cases := []struct {
		year  int16
		valid bool
	}{
		{
			year:  1899,
			valid: false,
		},
		{
			year:  1900,
			valid: true,
		},
		{
			year:  1901,
			valid: true,
		},
		{
			year:  2009,
			valid: true,
		},
		{
			year:  2010,
			valid: true,
		},
		{
			year:  2011,
			valid: false,
		},
	}

	for _, c := range cases {
		valid := ValidYear(c.year)

		if c.valid != valid {
			t.Errorf("We expected date %d to be a valid date", c.year)
		}
	}
}

func TestLeapYear(t *testing.T) {

	var leapYears = map[int16]bool{
		1892: true, 1896: true, 1904: true, 1908: true, 1912: true, 1916: true,
		1920: true, 1924: true, 1928: true, 1932: true, 1936: true, 1940: true,
		1944: true, 1948: true, 1952: true, 1956: true, 1960: true, 1964: true,
		1968: true, 1972: true, 1976: true, 1980: true, 1984: true, 1988: true,
		1992: true, 1996: true, 2000: true, 2004: true, 2008: true, 2012: true,
		2016: true, 2020: true,
	}

	for y := _minYear; y <= _maxYear; y++ {
		leap := LeapYear(y)
		if leap != leapYears[y] {
			t.Errorf("Expected leap year = %t for year %d", leap, y)
		}
	}
}

package calendar

import (
	"testing"
)

func TestDateImplementsDaterInterface(t *testing.T) {
	// Will cause a compile error if Table does not implement all methods defined in interface
	var _ Dater = NewDate(int16(2010), uint8(8), uint8(12))
}

func TestDaysFromEpoc(t *testing.T) {

	cases := []struct {
		date *Date
		days int
	}{
		{
			date: NewDate(1900, 01, 01),
			days: 0,
		},
		{
			date: NewDate(1900, 01, 02),
			days: 1,
		},
		{
			date: NewDate(1900, 12, 31),
			days: 364,
		},
		{
			date: NewDate(1901, 01, 01),
			days: 365,
		},
		{
			date: NewDate(1904, 01, 01),
			days: 1460,
		},
		{
			// https://www.timeanddate.com/date/durationresult.html?d1=01&m1=01&y1=1900&d2=31&m2=12&y2=2010
			date: NewDate(2010, 12, 31),
			days: 40541,
		},
	}

	for _, c := range cases {
		output := c.date.daysSinceEpoc()
		if c.days != output {
			t.Errorf("Expected %d days since epoc for date %s but received %d", c.days, c.date, output)
		}
	}
}

func TestDaysDiff(t *testing.T) {

	cases := []struct {
		dateOne *Date
		dateTwo *Date
		days    int
	}{
		{
			dateOne: NewDate(1995, 1, 8),
			dateTwo: NewDate(2005, 12, 24),
			days:    4003,
		},
		{
			dateOne: NewDate(1969, 4, 15),
			dateTwo: NewDate(1945, 9, 12),
			days:    8616,
		},
	}

	for _, c := range cases {
		output := c.dateOne.DaysDiff(c.dateTwo)
		if c.days != output {
			t.Errorf("Expected %d days", c.days)
		}
	}

}

func TestValidate(t *testing.T) {
	cases := []struct {
		year  int16
		month uint8
		day   uint8
		valid bool
	}{
		{
			year:  1899,
			month: 01,
			day:   1,
			valid: false,
		},
		{
			year:  1900,
			month: 1,
			day:   0,
			valid: false,
		},
		{
			year:  2000,
			month: 2,
			day:   31,
			valid: false,
		},
		{
			year:  2000,
			month: 13,
			day:   31,
			valid: false,
		},
		{
			year:  2000,
			month: 0,
			day:   31,
			valid: false,
		},
		{
			year:  2000,
			month: 6,
			day:   31,
			valid: false,
		},
		{
			year:  2000,
			month: 6,
			day:   30,
			valid: true,
		},
	}

	for _, c := range cases {
		valid := validate(c.year, c.month, c.day)
		if valid != c.valid {
			t.Errorf("Expected valid: %t date y:%d, m:%d, d:%d", c.valid, c.year, c.month, c.day)
		}
	}
}

func TestInvalidDateCreation(t *testing.T) {
	cases := []struct {
		year  int16
		month uint8
		day   uint8
	}{
		{
			year:  1899,
			month: 01,
			day:   1,
		},
		{
			year:  1900,
			month: 1,
			day:   0,
		},
		{
			year:  2000,
			month: 2,
			day:   31,
		},
		{
			year:  2000,
			month: 13,
			day:   31,
		},
		{
			year:  2000,
			month: 0,
			day:   31,
		},
		{
			year:  2000,
			month: 6,
			day:   31,
		},
	}

	for _, c := range cases {
		date := NewDate(c.year, c.month, c.day)
		if date != nil {
			t.Errorf("Expected nil object for date y:%d, m:%d, d:%d", c.year, c.month, c.day)
		}
	}
}

func TestString(t *testing.T) {
	cases := []struct {
		date   *Date
		output string
	}{
		{
			date:   NewDate(1900, 1, 1),
			output: "01 01 1900", // Pad both m & d
		},
		{
			date:   NewDate(1900, 10, 2),
			output: "02 10 1900", // Pad both d
		},
		{
			date:   NewDate(1900, 1, 20),
			output: "20 01 1900", // Pad both m
		},
		{
			date:   NewDate(1900, 10, 20),
			output: "20 10 1900", // No padding
		},
	}

	for _, c := range cases {

		output := c.date.String()
		if c.output != output {
			t.Errorf("String() failure. Expected %s but got %s", c.output, output)
		}
	}
}

func TestDateBefore(t *testing.T) {
	cases := []struct {
		date1  *Date
		date2  *Date
		before bool
	}{
		{
			date1:  NewDate(1900, 1, 1),
			date2:  NewDate(1900, 1, 1),
			before: false, // same
		},
		{
			date1:  NewDate(1900, 10, 2),
			date2:  NewDate(1900, 1, 1),
			before: false,
		},
		{
			date1:  NewDate(1900, 1, 1),
			date2:  NewDate(1900, 10, 2),
			before: true,
		},
	}

	for _, c := range cases {

		before := c.date1.Before(c.date2)

		if c.before != before {
			t.Errorf("Expected date %s to be before date %s", c.date1, c.date2)
		}
	}
}

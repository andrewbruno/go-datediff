package calendar

import (
	"fmt"
)

const (
	minMonth       = uint8(1)
	maxMonth       = uint8(12)
	leapYearDays   = 366
	commonYearDays = 365
)

// Date is the object that contains a date, as in a day of the year
type Date struct {
	year  int16
	month uint8
	day   uint8
}

// Dater represents the public functions that must be implemented
type Dater interface {
	DaysDiff(*Date) int
	String() string
	Before(*Date) bool
}

// NewDate creates the Date object.
// If values passed in are invalid, a nil object is returned.
// I have seen multiple patterns in returning objects that are invalid,
// e.g.  obj, bool, err where object may not be nil, but the valid bool is false.
// For simplicity, we return nil if dates passed in are invalid.
func NewDate(year int16, month uint8, day uint8) *Date {
	if validate(year, month, day) {
		return &Date{year: year, month: month, day: day}
	}
	return nil
}

// DaysDiff returns the number of days between itself (the receiver) and the passed in date.
// Returned valus is always postive, as it represents the difference.
func (d *Date) DaysDiff(date *Date) int {
	diff := d.daysSinceEpoc() - date.daysSinceEpoc()
	if diff < 0 {
		return diff * -1
	}

	return diff
}

// String returns a string representation in the format of DD MM YYYY
func (d *Date) String() string {
	var year, month, day string

	year = fmt.Sprintf("%d", d.year)

	if d.month < 10 {
		month = fmt.Sprintf("0%d", d.month)
	} else {
		month = fmt.Sprintf("%d", d.month)
	}

	day = fmt.Sprintf("%d", d.day)
	if len(day) == 1 {
		day = "0" + day
	}

	return fmt.Sprintf("%s %s %s", day, month, year)
}

// Before returns true if the receiver's date is before the passed in date
func (d *Date) Before(date *Date) bool {
	if d.daysSinceEpoc() < date.daysSinceEpoc() {
		return true
	}

	// if equal or greater
	return false
}

// Internal function to determine the number of days since the beginning.
func (d *Date) daysSinceEpoc() int {

	days := int(0)

	// Determine full years first.

	// _minYear is "our" epoc
	for year := int16(_minYear); year < d.year; year++ {
		if LeapYear(year) {
			days += leapYearDays
		} else {
			days += commonYearDays
		}
	}

	// Let's work out the edge days
	// e.g. 1900,1,2 = 1 day

	for m := uint8(1); m <= d.month-1; m++ {
		daysInMonth := daysInMonth(m, d.year)
		days += int(daysInMonth)
	}

	// Add the days of edge month
	return days + int(d.day) - 1
}

// validate returns true if the date is a valid date
func validate(year int16, month uint8, day uint8) bool {

	if !ValidYear(year) {
		return false
	}

	// Only allow months 1 - 12
	if month < minMonth || month > maxMonth {
		return false
	}

	daysInMonth := daysInMonth(month, year)

	if day < 1 || day > daysInMonth {
		return false
	}

	return true
}

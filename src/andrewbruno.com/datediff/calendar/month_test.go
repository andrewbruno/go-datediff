package calendar

import "testing"

func TestDaysInMonth(t *testing.T) {
	cases := []struct {
		month  uint8
		year   int16
		expect uint8
	}{
		{
			month:  1,
			year:   2000,
			expect: 31,
		},
		{
			month:  2,
			year:   2000,
			expect: 29,
		},
		{
			month:  2,
			year:   2001,
			expect: 28,
		},
		{
			month:  3,
			year:   2000,
			expect: 31,
		},
		{
			month:  4,
			year:   2000,
			expect: 30,
		},
		{
			month:  5,
			year:   2000,
			expect: 31,
		},
		{
			month:  6,
			year:   2000,
			expect: 30,
		},
		{
			month:  7,
			year:   2000,
			expect: 31,
		},
		{
			month:  8,
			year:   2000,
			expect: 31,
		},
		{
			month:  9,
			year:   2000,
			expect: 30,
		},
		{
			month:  10,
			year:   2000,
			expect: 31,
		},
		{
			month:  11,
			year:   2000,
			expect: 30,
		},
		{
			month:  12,
			year:   2000,
			expect: 31,
		},
	}

	for _, c := range cases {
		output := daysInMonth(c.month, c.year)
		if output != c.expect {
			t.Errorf("Recedived daysInMonth: %d for year %d, month %d.  Expected %d", output, c.year, c.month, c.expect)
		}
	}
}

package calendar

// daysInMonth returns the max number of days for a month in a particular year.
func daysInMonth(month uint8, year int16) uint8 {
	days := uint8(0)
	if month == 2 {
		if LeapYear(year) {
			days = 29
		} else {
			days = 28
		}
	} else if month == 4 || month == 6 || month == 9 || month == 11 {
		days = 30
	} else {
		days = 31
	}

	return days
}

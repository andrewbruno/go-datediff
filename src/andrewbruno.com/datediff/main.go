package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"andrewbruno.com/datediff/ui"
)

func main() {

	var (
		readerIn  = os.Stdin
		writerOK  = os.Stdout
		writerErr = os.Stderr
	)

	fmt.Fprint(writerOK, "\n-- Date Difference  -- \n\n")
	fmt.Fprint(writerOK, "Please enter your dates like this: DD MM YYYY, DD MM YYYY\n")
	reader := bufio.NewReader(readerIn)

	for {
		fmt.Fprint(writerOK, "Input> ")

		input, _ := reader.ReadString('\n')
		input = strings.Trim(input, "\n")
		input = strings.TrimSpace(input)

		if input != "" {
			if strings.ToLower(input) == "q" {
				fmt.Fprint(writerOK, "Goodbye\n")
				os.Exit(0)
			}

			date1, date2, err := ui.ParseInputDates(input)

			if err != nil {
				fmt.Fprintf(writerErr, "Output> Invalid Input: %s\n", input)
				continue
			}

			diff := date1.DaysDiff(date2)

			if date1.Before(date2) {
				fmt.Fprintf(writerOK, "Output> %s, %s, %d\n", date1, date2, diff)
			} else {
				fmt.Fprintf(writerOK, "Output> %s, %s, %d\n", date2, date1, diff)
			}
		}

	} // End for
}

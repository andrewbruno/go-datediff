# Date Diff

A Go (Golang) implementation as per [PROBLEM](doc/PROBLEM.md) requirements.

## Setup

1. Install Go 1.8+

2. Ensure GOPATH is set to root of the project

e.g. the following command should work

```
cd $GOPATH/src/andrewbruno.com/datediff
```

## Extra Features

CLI accepts "q" to quit program

## Testing via CLI

```
cd $GOPATH/src/andrewbruno.com/datediff
go run main.go 
```

## Testing via file input

```
$GOPATH/src/andrewbruno.com/datediff
go run main.go < invalid-cases.txt 
go run main.go < valid-cases.txt 
```

## Testing code coverage via GO

```
go test -cover ./...
```

First thing that comes to mind is leap year.  We are going to have 366 and 365 days.

https://en.wikipedia.org/wiki/Leap_year

"These extra days occur in years which are multiples of four (with the exception of years divisible by 100 but not by 400)"


Need a way to track the number of days in a particular month, e.g. 31, 28/29, 30
> KISS & YAGNI: No need for a hash map, which would require more test code.
>  Decided to use if m == 4 || m == 6 || m == 9 || m == 11 { days += 30 etc..

Consider using the real SDK API to bulild test cases - maybe
> No need, decided to use https://www.timeanddate.com/date/durationresult.html?d1=01&m1=01&y1=1900&d2=31&m2=12&y2=2010 to build test cases.


Retun Not Supported if out of bounds
> Spec does not say anything about what to do when out of bounds of an invalid date!!!

Need to add a Date Validator
> As they could put, 2010, 02, 35 ... where 35 is invalid

build a parses, that returns the objects, day, year, month
e.g. d, m, y, e := calendar.parse("")

Create you own Calendar objects.

Year
Month
Day 

But they ask for MINIMUM Amount of code, but elegant.  This can be subjective mmm.

-- Consider createding a 3 level deep for loop, and test all date ranges
    for y 1900 to 2010
       for m 1 to 12
         for d 1 to 31
      